FROM continuumio/miniconda3:4.3.14

ENV APP_HOME /app

RUN mkdir $APP_HOME
WORKDIR $APP_HOME

ADD ./environment.yml $APP_HOME
RUN conda env update --quiet -n root -f environment.yml

RUN useradd -ms /bin/bash webapp && touch /.condarc && chown webapp:webapp /.condarc && mkdir /var/log/app && chown webapp:webapp /var/log/app
ADD . $APP_HOME
RUN /bin/bash -c "pwd && flake8 . && nosetests ."
EXPOSE 5000

USER webapp
CMD /bin/bash -c "gunicorn --log-file=- -c gunicorn.py -w 4 --bind 0.0.0.0:5000 pinpoint_service:app"
