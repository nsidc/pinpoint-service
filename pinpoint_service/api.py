from flask import jsonify
import logloglog
import requests

from pinpoint_service import coord_converter
from pinpoint_service.coord_converter import LocatorException

log = logloglog.init('pinpoint_service', 'logging.yml')


def root_response():
    return ("For documentation see <a href='./ui/'>the swagger documentation ui</a>")


def calculate_coords(latitude=None, longitude=None, gpd_name=None, format='json'):
    """ Docstring goes here"""
    try:
        x, y = coord_converter.column_row_from_lat_long(latitude, longitude, gpd_name)

    except LocatorException as e:
        return e.msg, requests.codes.internal_server_error
    return jsonify(vertical=y, horizontal=x)
