import json
from unittest import TestCase


from pinpoint_service import app


class Test_Root(TestCase):
    def setUp(self):
        self.app = app.app.test_client()

    def test_root_response(self):
        response = self.app.get('/')
        self.assertEquals(response.status, '200 OK')
        response_string = str(response.data, encoding='utf8')
        self.assertIn(response_string, "For documentation see <a href='./ui/'>"
                      'the swagger documentation ui</a>')


class Test_map(TestCase):
    def setUp(self):
        self.app = app.app.test_client()

    def test_returns_400_bad_request_with_missing_parameters(self):
        response = self.app.get('/convert')
        self.assertEquals(response.status, '400 BAD REQUEST')

    def test_returns_coords_with_valid_parameters(self):
        response = self.app.get('/convert?latitude=0&longitude=0'
                                '&gpd_name=EASE2_M09KM')
        expected = {'horizontal': 1927.5, 'vertical': 811.5}

        self.assertEquals(response.status, '200 OK')
        self.assertEquals(json.loads(response.data), expected)
