import os
import numpy as np
from unittest import TestCase

from pinpoint_service.coord_converter import LocatorException
from pinpoint_service.coord_converter import column_row_from_lat_long as column_row_from_lat_long


class Test_coord_converter(TestCase):
    def test_verify_EASE_2_M9KM_from_lat_long(self):
        lats, lons = self.get_lats_lons_from_fixture('EASE2_M09km.lats.3856x1624x1.double',
                                                     'EASE2_M09km.lons.3856x1624x1.double',
                                                     3856, 1624)
        self.validate_coord_conversion(lats, lons, 'EASE2_M09KM')

    def test_verify_EASE_2_N9KM_from_lat_long(self):
        lats, lons = self.get_lats_lons_from_fixture('EASE2_N09km.lats.2000x2000x1.double',
                                                     'EASE2_N09km.lons.2000x2000x1.double',
                                                     2000, 2000)
        self.validate_coord_conversion(lats, lons, 'EASE2_N09KM')

    def test_verify_EASE_2_S9KM_from_lat_long(self):
        lats, lons = self.get_lats_lons_from_fixture('EASE2_S09km.lats.2000x2000x1.double',
                                                     'EASE2_S09km.lons.2000x2000x1.double',
                                                     2000, 2000)
        self.validate_coord_conversion(lats, lons, 'EASE2_S09KM')

    def test_raises_exception_with_no_gpd_defined(self):
        with self.assertRaises(LocatorException):
            column_row_from_lat_long(0, 0)

    def test_raises_exception_with_invalid_gpd_defined(self):
        with self.assertRaises(LocatorException):
            column_row_from_lat_long(0, 0, 'foobar')

    def validate_coord_conversion(self, lats, lons, gpd):
        for y in range(lats.shape[0])[::24]:
            for x in range(lats.shape[1])[::24]:
                print('Testing {}, {}'.format(x, y))
                lat = lats[y, x]
                lon = lons[y, x]
                if -999.0 in [lat, lon]:
                    continue

                coord_x, coord_y = column_row_from_lat_long(lats[y, x],
                                                            lons[y, x],
                                                            gpd_name=gpd)
                self.assertAlmostEquals(coord_x, x, places=6)
                self.assertAlmostEquals(coord_y, y, places=6)

    def get_lats_lons_from_fixture(self, lat_filename, lon_filename, x_size, y_size):
        grids_dir = os.path.join(os.path.dirname(__file__), 'grids/')
        lats = np.fromfile(os.path.join(grids_dir,
                                        lat_filename),
                           dtype=np.float64).reshape(y_size, x_size)
        lons = np.fromfile(os.path.join(grids_dir,
                                        lon_filename),
                           dtype=np.float64).reshape(y_size, x_size)

        return (lats, lons)
