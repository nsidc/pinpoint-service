# License
This project is licensed under the terms of the MIT License.   See LICENSE.txt

# Pinpoint
This project is a flask/python microservice that translates lat/lon values to
i,j center-of-grid cell coordinates for all NSIDC EASE2 grids

## Development Quickstart

This project uses [conda](http://conda.pydata.org/miniconda.html) python. It must be installed to run the tool.

### Running project locally:

1. Clone the repository
2. `cd pinpoint-service`
3. `conda env create -f environment.yml`
4. `. activate pinpoint_service`
5. `python run_server.py`


### Running on containers

With docker installed locally, run 'docker-compose up' from the project root.  This will start the app in flask dev mode (reloads on code change) on port 80 on your local machine.  ***Please Note*** -- flask/connexion does not support auto-reload when the swagger.yml changes.  If you're adding routes/modifying the spec, you'll need to restart the app.


## Development/Release Workflow

This project uses [github flow](https://guides.github.com/introduction/flow/). To begin a feature, start a branch and follow semvar guidelines for versioning.  [bumpversion](https://github.com/peritus/bumpversion) is used to keep the versions in sync across the files in the project.

### Continuous Integration

This project uses [circle CI](https://circleci.com) for CI.

On any commit, Circle will build the container, the test commands `nostests` and `flake8` are currently built-in to the process via the Dockerfile

## Projections:

[EPSG:6933](https://epsg.io/6933) -- WGS84/Ease 2.0 Global

[EPSG:6932](https://epsg.io/6932) -- WGS84/Ease 2.0 South

[EPSG:6931](https://epsg.io/6931) -- WGS84/Ease 2.0 North

## Important Links:

* [Bitbucket repo][thisrepo]
* [Jira][jira]
* [Trello][trello]


[trello]: https://trello.com/b/s8ioPirr/morf
[thisrepo]: https://bitbucket.org/nsidc/pinpoint-service
[jira]: https://nsidc.org/jira/browse/AP-58
